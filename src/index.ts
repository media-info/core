import deepmerge from 'deepmerge';
import {MediaInfoModule} from '@media-info/types';

function isDefined<T>(argument: T | undefined): argument is T {
    return argument !== undefined
}

type MediaType = 'movie' | 'tvshow' | 'season' | 'episode';

export class MediaInfoCore<T1 extends MediaInfoModule,
    T2 extends MediaInfoModule,
    T3 extends MediaInfoModule,
    T4 extends MediaInfoModule,
    T5 extends MediaInfoModule,
    T6 extends MediaInfoModule,
    T7 extends MediaInfoModule,
    T8 extends MediaInfoModule,
    T9 extends MediaInfoModule,
    T10 extends MediaInfoModule,
    T11 extends MediaInfoModule,
    T12 extends MediaInfoModule,
    > {

    modules: Array<T1 | T2 | T3 | T4 | T5 | T6 | T7 | T8 | T9 | T10 | T11 | T12>;

    constructor(module1?: T1,
                module2?: T2,
                module3?: T3,
                module4?: T4,
                module5?: T5,
                module6?: T6,
                module7?: T7,
                module8?: T8,
                module9?: T9,
                module10?: T10,
                module11?: T11,
                module12?: T12,
    ) {
        this.modules = [...arguments];
    }

    async get<TMediaType, >(id: number, mediaType: TMediaType | MediaType): Promise<ReturnType<T1['get']>
        & ReturnType<T2['get']>
        & ReturnType<T3['get']>
        & ReturnType<T4['get']>
        & ReturnType<T5['get']>
        & ReturnType<T6['get']>
        & ReturnType<T7['get']>
        & ReturnType<T8['get']>
        & ReturnType<T9['get']>
        & ReturnType<T10['get']>
        & ReturnType<T11['get']>
        & ReturnType<T12['get']>
        | undefined> {
        const modulesData = await Promise.all(this.modules.map(m => m.get(id, mediaType)));
        if (!modulesData.length) return;
        const filtered = modulesData.filter(isDefined);
        return filtered.reduce((acc, d) => {
            return deepmerge(acc, d);
        }, filtered[0]);
    }
}
